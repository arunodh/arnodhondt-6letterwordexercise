﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1 {
  class Program {
    static void Main(string[] args) {
      Console.WriteLine("Welcome to 6 letter word exercise.");
      string path = Directory.GetCurrentDirectory();
      Console.WriteLine($"Reading from  {path}.");
      FileReader fileReader = new FileReader();
      List<string> lines = fileReader.ReadLinesFromFile("input.txt");
      WordFinderService wordFinderService = new WordFinderService();

      Console.Write("Please enter the length of the words and press enter: ");
      string choice = Console.ReadLine();
      int number;
      while (!Int32.TryParse(choice, out number)) 
      {
        Console.Write("Please enter a valid number: ");
        choice = Console.ReadLine();
      }

      Console.WriteLine("Searching all possible words..");

      List<string> foundWords = wordFinderService.GetWordsHiddenInSource(lines, number);
      Console.WriteLine("Found {0} words for the given parameters.", foundWords.Count);
      foreach (string line in foundWords) 
      {
        Console.WriteLine(line);
      }

      Console.ReadLine();
    }
  }
}
