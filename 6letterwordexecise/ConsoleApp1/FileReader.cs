﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1 {
  public class FileReader {

    public FileReader() 
    {
      Root = Path.Combine(Directory.GetCurrentDirectory(), "InputData");
    }
    public FileReader(string directory) 
    {
      Root = directory;
    }

    public string Root { get; set; }

    public List<string> ReadLinesFromFile(string fileName)
    {
      List<string> lines = new List<string>();
      string path = Path.Combine(Root, fileName);
      if (!Directory.Exists(Root)) {
          Directory.CreateDirectory(Root);
      }

      if (!File.Exists(path)) 
      {
        return lines;
      }

      string newLine;
      StreamReader fileReader = new StreamReader(path);
      while ((newLine = fileReader.ReadLine()) != null) 
      {
        lines.Add(newLine);
      }

      return lines;
    }
  }
}
