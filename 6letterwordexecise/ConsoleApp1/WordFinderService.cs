﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1 {
  public class WordFinderService {
    public WordFinderService() {
    }

    public List<string> GetWordsHiddenInSource(List<string> source, int maxWordLength) {
      List<string> possibleWords = new List<string>();
      List<string> possibleComboWord = new List<string>();
      List<string> resultWords = new List<string>();
      foreach (string word in source) {
        if (word.Length == maxWordLength) {
          possibleWords.Add(word.Trim());
        }
        else if (word.Length < maxWordLength) {
          possibleComboWord.Add(word.Trim());
        }
      }

      possibleComboWord = possibleComboWord.Distinct().ToList();

      foreach (string word in possibleComboWord) {
        List<string> temp = possibleComboWord.Where(_ => _.Length + word.Length == maxWordLength).ToList();
        temp.RemoveAll(_ => _ == word);
        foreach (string appString in temp) {
          string possComb = string.Concat(word, appString);
          if (possibleWords.Contains(possComb)) {
            resultWords.Add($"{word}+{appString}={possComb}");
          }
        }
      }

      return resultWords;
    }
  }
}

